"""test package for pyslk."""

import subprocess
import tempfile

import pytest


def _cmlorskip(command):
    try:
        commands = [command, "version"]
        subprocess.run(commands, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        has = True
    except FileNotFoundError:
        has = False
    func = pytest.mark.skipif(not has, reason="requires slk")
    return has, func


has_slk, requires_slk = _cmlorskip("slk")

scratch = tempfile.mkdtemp()
