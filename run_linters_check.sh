#!/bin/bash

black --check -t py310 pyslk --diff
isort --check --profile black pyslk
flake8 pyslk
