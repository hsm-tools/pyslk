#!/bin/bash

# check if proper number of arguments is supplied
if [ "$#" -ne 1 ]; then
    >&2 echo "ERROR, `date`: need one input arguments (got $#): <tag to be deleted>"
    exit 2
fi

# get tag
tag_to_delete=$1

# user info
echo "deleting tag '${tag_to_delete}'"

# delete local tag
git tag -d ${tag_to_delete}

# delete remote tag
git push --delete origin ${tag_to_delete}

# done
exit 0

