# Changes

## 2.2.10 (2025-03-03)

* updated install method in readme
* updated install method and availability in HTML doc


## 2.2.9 (2025-03-03)

* accidentally, tag 2.2.8 was push before changes of version 2.2.8 were pushed; fixed now


## 2.2.8 (2025-03-03)

* updates Readme


## 2.2.7 (2025-02-25)

* updates Readme


## 2.2.6 (2025-02-25)

* updated LICENCe
* published as Test PyPI


## 2.2.5 (2025-02-21)

* removed debugging output


## 2.2.4 (2025-02-18)

* changes/corrections in build and documentation files
* updated tests


## 2.2.3 (2025-02-13)

* minor changes/corrections in build and documentation files


## 2.2.2 (2025-02-12)

* no changes for end-users (only for developers)
* improved the installation setup, build files, CI/CD workflow:
  * dynamic versioning in pyproject.toml and documentation
  * generate `meta.yaml` via `grayskull` from pypi `*.tar.gz` package
  * generate wheel in addition to pypi `*.tar.gz`
  * conda packages are now `*.conda` instead of `*.tar.bz2` files
  * conda package is now `noarch` instead of `linux_64` + `py<VERSION>` => one conda package per pyslk version
  * `doc/changelog.rst` has been removed and `CHANGES.md` is directly included in HTML documentation
  * `uv build` replaces `python build`
  * use git tags in in `.gitlab-ci.yml` instead of reading version from a code file
  * `doc/conf.py` contains still two versions to be set manually: versions of `pyslk` installed on Levante => variables `version_unstable` (module: `python3/unstable`) and `version stable` (most recent module: `python3/202[0-9]....`)


## 2.2.1 (2025-02-04)

* fixed import issue in archival modules
* adapted code to `black` version 25.1.0
* conda packages are generated as `*.conda` files; formerly: `*.tar.bz2`; see for details: https://docs.conda.io/projects/conda-build/en/stable/concepts/package-naming-conv.html


## 2.2.0 (2024-12-06)

* adapted to new `slk_helpers` version 1.13.0 but not to all new features
* adapted signatures of existing functions:
  * `group_files_by_tape`
  * `group_files_by_tape_raw`
  * `list_clone_file`
  * `list_clone_file_raw`
* new functions:
  * `recall_single`: calls `slk_helpers recall`
  * `retrieve_improved`: calls `slk_helpers retrieve`
  * `recall2_raw`: calls `slk_helpers recall` (raw warpper; better use `recall_single`)
  * `retrieve2_raw`: calls `slk_helpers retrieve` (raw warpper; better use `retrieve_improved`)


## 2.1.3 (2024-10-07)

* fixed an error resulting from change in version 2.1.1


## 2.1.2 (2024-10-07)

* skipped due to workflow issue


## 2.1.1 (2024-09-26)

* fixed issue related to conversion of dates when no `locale` is set


## 2.1.0 (2024-08-05)

* renamed internal modules `warnings` and `exceptions` to `pyslk_warnings` and `pyslk_exceptions`, respectively
* fixed conda and pypi package builds


## 2.0.0 (2024-04-25)

* each `slk_helpers` command has a respective method in `pyslk` -- partly, multiple functions are available
* `slk archive`, `slk retrieve` and `slk recall` are still not properly reflected in `pyslk`
* adapted to all relevant features of `slk_helpers` v1.12.10
* removed multiple deprecated functions (see documentation on what is available or ask support@dkrz.de)
* adapted types of a few thrown errors (issue #64)
* completely new internal structure


## 1.9.5 (2023-10-17)

* improved type usage
* basic `*_raw` wrapper functions for commands newly introduced in `slk_helpers` version 1.9.9
    * `search_immediately_raw`
    * `search_incomplete_raw`
    * `search_successful_raw`
    * `submit_verify_job_raw`
    * `job_report_raw`
    * `print_rcrs_raw`
* no comfortable wrappers for new `slk_helpers` commands yetexcept for
    * `is_search_incomplete`
    * `is_search_successful`
* fixed an error type

## 1.9.4 (2023-10-17)

* doc updates
* new function `pyslk.construct_dst_from_src()` which accepts source file(s) and a destination root path is input and constructs the destination path of each source file if it was archived by `slk archive`

## 1.9.3 (2023-09-04)

* all functions which accept paths and filenames as input accept 'Path-like' objects now
* a few output types where corrected
* the type checking in multiple functions was extended yields clearer error messages now
* extended text of deprecated warnings
* added deprecated info to all doc strings
* added some more 'see also' to the doc strings
* function `json_str2hsm` was not implemented before
* fixed type-checking and output types of `hsm2json`, `hsm2json_dict` and `hsm2json_file`
* updated errors and error messages
* major updates in the documentation for #35
* removed python 3.8 conda build; fixed #63
* minor other corrections

## 1.9.2 (2023-08-24)

* updated auto-generated documentation page
* adapted to slk_helpers versions 1.9.6 and 1.9.7
* split `pyslk.mkdir` into `pyslk.mkdir` and `pyslk.makedirs` similar to `os`
* fixed type checks and output types in/of a few functions
* changed default value of `preserve_path` in `pyslk.retrieve_raw` to `True`

## 1.9.1 (2023-07-13)

* corrections in changelog
* updates in readme
* minor changes in retry function and error handling

## 1.9.0 (2023-07-07)

* many new functions in `core` (see doc):
* input arguments of several functions of `core` changed:
  * `cached`
  * `iscached`
* many new functions in `raw` (not listed)
* most functions starting with `slk_` are deprecated
* some functions in `utils` made public
  * `convert_expiration_date` (new in pyslk 1.9.0)
  * `login_valid`
  * `run_slk`
* adapted to `slk_helpers` 1.7.5:
  * new parameter `print_hidden` for functions related to commands `metadata` and `hsm2json`
  * new parameter `json_string` for functions related to command `json2hsm`

## 1.8.1 (2023-08-08)

* maintenance release while 1.9.x is already available
* fixed function `iscached`
* added Python 3.11 conda package build
* updated `.gitlab-cy.yml`

## 1.8.0 (2023-02-13)

* new function `search_simple`

## 1.7.0 (2023-02-10)

* adapted exception handling for errorcodes for `exists` and `iscached` (!49).
* new module `raw` (will replace module `pyslk`) in future
* new module `core`
  * functions from `parsers` moved to `core` and renamed
  * new nice wrapper functions in `core`
  * function names in `core` are equal to slk/slk_helpers commands
* removed version checks and user warning when old slk/slk_helpers versions used

## 1.6.3 (2023-02-06)

* changed `slk_helpers` version number to 1.7.3

## 1.6.2 (2023-02-02)

* changed version number to 1.7.2 (`slk_helpers`)
* syntax changes to pass black

## 1.6.1 (2023-01-31)

* changed version numbers to 1.7.1 (`slk_helpers`) and 3.3.83 (`slk`)

## 1.6.0 (2023-01-16)

* adapted to `slk_helpers` 1.7.1 and to `slk` 3.3.81
* new functions `parsers.slk_job_exists_bool` and `parsers.slk_tape_exists_bool`
* changes in `parsers.slk_exists_bool`
* removed forgotten debugging output from `pyslk.slk_search`

## 1.5.0 (2022-12-08)

* `parsers.slk_list_formatted` returns an empty pandas.DataFrame if `slk list` prints no results.
* new `config` module
* several updates relevant for developers of `pyslk`
* updated documentation
* adapted to slk 3.3.76
  * no new parameters and functions
  * mainly bug fixes and similar
  * details here: https://docs.dkrz.de/blog/2022/changelog-slk-3-3-76.html
* slk_helpers 1.6.0
  * new commands: `tape_exists`, `tape_status`, `group_files_by_tape` => wrappers `pyslk.slk_*`
  * details here: https://docs.dkrz.de/blog/2022/changelog-slkhelpers-1-6-0.html

## 1.4.1 (2022-11-15)

* fixed call of `slk_helpers`

## 1.4.0 (2022-10-18)

* removing `pyslk.slk_total_number_search_results` again
* updated to `slk` 3.3.67 and `slk_helpers` 1.5.7

## 1.3.1 (2022-10-17)

* corrected error in version number

## 1.3.0 (2022-10-17)

* updated to new `slk_helpers` version 1.5.6
* new function `pyslk.slk_total_number_search_results` wrapping `slk_helpers total_number_search_results`

## 1.2.2 (2022-10-12)

* updated to new slk and slk_helpers version numbers (3.3.64 and 1.5.3)
* include new parameters of `hsm2json` and `list_search`

## 1.2.1 (2022-10-04)

* updated version in README and documentation
* `hsm2json` has new argument `print_summary`
* `pyslk.parsers.slk_list_formatted` works with slk 3.3.21 and 3.3.61

## 1.2.0 (2022-10-04)

* adapted to slk 3.3.61 and slk_helpers v1.5.1
  * rename `slk_import_metadata` and `slk_export-metaddata` to `json2hsm` and `hsm2json`
  * removed arguments from `json2hsm`
  * new write mode for metadata: `CLEAN` => delete all metadata of targeted metadata schema prior to writing new metadata to these schemata
* updated error messages

## 1.1.0 (2022-09-27)

* adapted to slk 3.3.56 and slk_helpers v1.4.0
  * merged the all commands `import_metadata_*` to `import_metadata`
  * remove argument `fully_quiet` of `import_metadata` and `export_metadata` commands
  * new arguments of `import_metadata`
* new parsers functions

## 1.0.0 (2022-09-19)

* compatible with slk v3.3.46 and slk_helpers 1.3.2 (also mostly with >=1.3.0)
  * `parsers.slk_list_formatted` is not compatible with slk v3.3.21
  * new input arguments of `slk retrieve` and `slk archive`
  * output of `slk list` is differently formatted (and, thus, output of `pyslk.slk_list` also different)
* renamed argument `all` of function `pyslk.slk_list` and `parsers.slk_list_formatted` to `show_hidden`
* removed argument `column_widths` from `slk_list_search_formatted` (slk list now has variable col width)
* minor bug fixes in `parsers.slk_list_formatted`
* changed setup method from setup.py to pyproject.toml


## 0.5.9 (2022-05-21)

* adapted to slk_helpers 1.2.x
* updated examples in doc
* minor bug fixes


## 0.5.8 (2022-05-12)

* added new functions to generate search queries from a file list


## 0.5.7 (2022-04-14)

* minor changes in the package publishing workflow


## 0.5.6 (2022-04-14)

* removed argument `return_format` from `pyslk.slk_arch_size` (was accidentally kept in previous version)


## 0.5.5 (2022-03-21)

* `utils._parse_size(...)` returns a `math.nan` if a file size cannot be converted
* fucntions that use `utils._parse_size(...)` were adapted
* Thanks to Lars Buntemeyer (GERICS) and Helge Heuer (DLR) for contributions to 0.5.x


## 0.5.4 (2022-03-21)

* new function `pyslk.slk_arch_size(...)` simplified (removed "format" argument)
* new function `parsers.slk_arch_size_format(...)`
* internal restructuring of modules `parsers` and `utils`
* moving functions between `parsers` and `utils`
* `Pandas` is not required for the whole parsers module
* new module `constants` to host constants


## 0.5.3 (2022-03-18)

* new function `pyslk.slk_arch_size(...)` (contributed by Helge Heuer, DLR)
* updated doc: pyslk installed on levante


## 0.5.2 (2022-03-14)

   * minor version number fixes


## 0.5.1 (2022-03-14)

   * build conda packages for python 3.9 and 3.10


## 0.5.0 (2022-03-14)

* `slk group` is activated now
* `pyslk` extended by new `slk_helpers` commands: `version`, `iscached` and `search_limited`; correspond to `pyslk.slk_helpers_version(...)`, `pyslk.slk_iscached(...)` and `pyslk.slk_search_limited(...)`
* path containing `//`, `/../` and `/./` now properly work with all commands
* `parsers` extended by `parsers.slk_isached_bool` and `parsers.slk_search_limited_int`


## 0.4.0 (2021-11-14)

* argument updates in pyslk.pyslk (see below)
* argument updates in pyslk.parsers (see below)
* removed non-existing arguments in  pyslk.pyslk.recall
* argument name changes (everywhere, see above):
   * R => recursive
   * n => numeric_ids
   * x => exclude_hidden
   * a => all
   * p => preserve_permissions
* pyslk.parsers.slk_list_formatted considerably enhanced
* doc updates


## 0.3.5 (2021-11-10)

* activated slk retrieve (`pyslk.slk_retrieve`)
* throw proper `PySlkException` when `slk` or `slk_helpers` are not available


## 0.3.4 (2021-11-01)

* minor corrections in the documentation
* deactivated slk retrieve (`pyslk.slk_retrieve`)


## 0.3.3 (2021-10-25)

* minor corrections in the documentation


## 0.3.2 (2021-10-23)

* minor editorial corrections and addons in the descriptions and readme


## 0.3.1 (2021-10-23)

* updated doc structure
* modified package description in `__init__.py`
* updated README.md


## 0.3.0 (2021-10-23)

* masked `pyslk.slk_search` and `pyslk.slk_group` because they do not exist in the slk version for public release (will come later with an update)
* updated descriptions
* code layout updates after flake8 checks
* basic structure for doc generation


## 0.2.2 (2021-10-02)

* new function in module `parsers.slk_exists_bool()`


## 0.2.1 (2021-10-02)

* new functions in module `parsers.valid_token()` and `parsers.valid_session()` (synonyms)


## 0.2.0 (2021-10-01)

* package setup:
   * setup.py, setup.cfg
   * `requirements*.txt` files
   * LICENCE
   * README.md
* finalized all slk functions (group, owner, chmod, tag)
* split one large source file into several modules
* updates in some function headers (`pyslk.slk_delete`, `pyslk.slk_tag`)
* several minor bug fixes


## 0.1.0 (2021-09-20)

Initial Release
