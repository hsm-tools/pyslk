# flake8: noqa
import warnings

__all__ = ["SlkWarning"]


class SlkWarning(Warning):
    pass
