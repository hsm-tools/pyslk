# flake8: noqa
# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

import os
import re
import shutil
import sys
import warnings

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import mock

MOCK_MODULES = ["pandas", "psutil", "tenacity"]
for mod_name in MOCK_MODULES:
    sys.modules[mod_name] = mock.Mock()
import pyslk

sys.path.insert(0, os.path.abspath(".."))

# -- Project information -----------------------------------------------------

project = "pyslk"
copyright = "2024, Deutsches Klimarechenzentrum GmbH"
author = "DKRZ"
html_logo = "_static/DKRZ_Logo.svg"
html_favicon = "_static/favicon.ico"

# The version info for the project you're documenting, acts as replacement
# for |version| and |release|, also used in various other places throughout
# the built documents.
#
## The full version, including alpha/beta/rc tags.
release = pyslk.__version__
# The short X.Y version.
version = re.match(r"^\d+\.\d+\.\d+", release).group(0)
version_stable = "2.2.10"
version_unstable = "2.2.10"

autosummary_generate = True
numpydoc_class_members_toctree = True
numpydoc_show_class_members = False


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ["sphinx.ext.autodoc", "sphinx.ext.autosummary", "numpydoc", "myst_parser"]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# The suffix(es) of source filenames.
# You can specify multiple suffix as a list of string:
#
# source_suffix = ['.rst', '.md']
# source_suffix = ".rst"
source_suffix = {
    ".rst": "restructuredtext",
    ".md": "markdown",
}

# The master toctree document.
master_doc = "index"

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This patterns also effect to html_static_path and html_extra_path
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store", "**.ipynb_checkpoints"]


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "sphinx_book_theme"
# html_theme = 'alabaster'

html_theme_options = dict(
    repository_url="https://gitlab.dkrz.de/hsm-tools/pyslk",
    repository_branch="master",
    path_to_docs="docs",
    use_edit_page_button=True,
    use_repository_button=True,
    use_issues_button=True,
    home_page_in_toc=False,
    footer_start=["imprint", "copyright", "sphinx-version"],
)

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]

if not os.path.exists("../build/html/_downloads"):
    pass

src_files = [
    f"../dist/pyslk-{version}.tar.gz",
    f"../dist/pyslk-{version}-py2.py3-none-any.whl",
    f"../conda/pyslk-{version}-py_0.conda",
]
dst_path = "../build/html/_downloads"
if not os.path.exists(dst_path):
    os.makedirs(dst_path)

for iF in src_files:
    if not os.path.isfile(iF):
        warnings.warn(f"Package file does not exist: {iF}")
    else:
        shutil.copyfile(iF, os.path.join(dst_path, os.path.basename(iF)))

rst_prolog = """
    .. |version_stable| replace:: %s

    .. |version_unstable| replace:: %s

    .. |download1| raw:: html

        <ul>
            <li><a href="_downloads/pyslk-%s-py2.py3-none-any.whl">pyslk-%s-py2.py3-none-any.whl</a></li>
            <li><a href="_downloads/pyslk-%s.tar.gz">pyslk-%s.tar.gz</a></li>
        </ul>

    .. |download2| raw:: html

        <ul>
            <li><a href="_downloads/pyslk-%s-py_0.conda">pyslk-%s-py_0.conda</a></li>
        </ul>

""" % (
    version_stable,
    version_unstable,
    version,
    version,
    version,
    version,
    version,
    version,
)
