Availability
=============

Levante
-------

``pyslk`` is available on Levante in the module ``python3/2023.01-gcc-11.2.0`` (pyslk version |version_stable|) and in ``python3/unstable`` (pyslk version |version_unstable|).

If you want to use pyslk in a personal conda environment on Levante, please have a look for the conda package further below.


DKRZ Jupyter Hub
----------------

If you never used the DKRZ Jupyter Hub instance, please have a look into the documentation of this service to learn about the first steps: https://jupyterhub.gitlab-pages.dkrz.de/jupyterhub-docs/ .


common usage: using an existing kernel
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Note from 2022-09-19: the default kernel mentioned below currently does not provide slk support. This will be fixed soon.

Please use the kernel ``Python3`` (based on the module ``python3/2023.01`` or ``python3/unstable``) to have ``pyslk`` and ``slk`` available.


expert usage: set up your own kernel
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you want to run your own kernel, ``slk`` has to be available (``pyslk`` just calls the command line interface ``slk``) and you need a Python environment with install ``pyslk``.

The ``PATH`` environment variable has to be set appropriately in order to make the ``slk`` available. E.g. do this first:

.. code-block:: bash

    module load slk slk_helpers

You can then create your own kernel via"

.. code-block:: bash

    python -m ipykernel install --name slk --display-name "hsm kernel" --env PATH $PATH --env JAVA_HOME $JAVA_HOME --user

Please note that the path of module might change when the module tree on Levante is re-created. If you experiance any issues which seem to be related to non-existing files, please re-created your kernel.

.. _dl_instll_pyslk:

Installation
------------------------------------

.. note::

    ``pyslk`` is no stand-alone-package. Instead, ``pyslk`` needs the command line interfaces ``slk`` and ``slk_helpers`` to be installed. ``slk`` is only available on selected systems at DKRZ and not available for download.


Dependencies
^^^^^^^^^^^^^^^^^

The installation requires the following dependencies

* :py:mod:`>= python 3.9`
* :py:mod:`>= slk 3.3.76` (``>= slk 3.3.90`` recommended; no Python package but and CLI on Levante)
* :py:mod:`>= slk_helpers 1.13.0` (``>= slk_helpers 1.13.3`` recommended; no Python package but and CLI on Levante)
* :py:mod:`>= pandas 1.4.0`
* :py:mod:`psutil`
* :py:mod:`>= python-dateutil 2.8.2`
* :py:mod:`>= tenacity 8.0.1`


Install from PyPI
^^^^^^^^^^^^^^^^^^^^^^^^^

The package can be installed via pip:

.. parsed-literal::

    pip install pyslk


If a specific version is required:

.. parsed-literal::

    pip install pyslk==2.2.9:


If you wish to upgrade to the most recent release:

.. parsed-literal::

    pip install pyslk==|version|


pip package
^^^^^^^^^^^^^^^^^

Files:

.. * :download:`pyslk-2.2.1.tar.gz<../dist/pyslk-2.2.1.tar.gz>`

.. from: https://groups.google.com/g/sphinx-users/c/kUtDImCmCuA/m/CjhVid8ISyEJ

|download1|

Install version:

.. from: https://stackoverflow.com/a/27418207/4612235

.. parsed-literal::

    pip install pyslk-|version|.tar.gz



Conda packages
^^^^^^^^^^^^^^^^^^^^^^^^

File:

|download2|

Install version:

.. parsed-literal::

    # downloaded file
    conda install --use-local  pyslk-|version|-py_0.conda




Other releases
^^^^^^^^^^^^^^^^^^^^^^^^

Several releases from version 0.3.3 onwards are available via a GitLab Package Registry (login with DKRZ account required):

https://gitlab.dkrz.de/hsm-tools/pyslk/-/packages


.. _`back to index page`: index.html
