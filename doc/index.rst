.. pyslk documentation master file, created by
   sphinx-quickstart on Sat Oct 23 22:03:13 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pyslk's documentation!
=================================

:py:mod:`pyslk` is a python wrapper for the command line interface
of StrongLink, the software of the HSM tape archival system at DKRZ.
:py:mod:`pyslk` wrapps two command line interfaces, ``slk`` for basic interaction
with the tape system and ``slk_helpers`` which serves as a DKRZ specific
extension of ``slk``.

This wrapper is compatible with slk versions ``>= 3.3.76`` (recommended: ``>= 3.3.90``)
and slk_helpers versions ``>= 1.13.0`` (some functions won't work; recommended: ``>= 1.13.3``).

Installation
-------------

The installation requires the following dependencies

* :py:mod:`>= python 3.9`
* :py:mod:`>= slk 3.3.76` (``>= slk 3.3.90`` recommended; no Python package but and CLI on Levante)
* :py:mod:`>= slk_helpers 1.13.0` (``>= slk_helpers 1.13.3`` recommended; no Python package but and CLI on Levante)
* :py:mod:`>= pandas 1.4.0`
* :py:mod:`psutil`
* :py:mod:`>= python-dateutil 2.8.2`
* :py:mod:`>= tenacity 8.0.1`


Download and installation of the library are described :ref:`here<dl_instll_pyslk>`.


.. toctree::
   :maxdepth: 1
   :caption: Contents:

   examples
   availability
   api
   CHANGES
   development


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
