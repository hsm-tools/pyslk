.. currentmodule:: pyslk

#############
API reference
#############

This page provides an auto-generated summary of the pyslk API.

Wrapper Functions which return nice output
============================================

Wrappers of ``slk`` and ``slk_helpers`` commands with improved output.

.. autosummary::
   :toctree: generated/

   access_hsm
   arch_size
   archive
   chgrp
   chmod
   chown
   cli_versions
   count_tapes
   count_tapes_with_multi_tape_files
   count_tapes_with_single_tape_files
   delete
   expiration_date
   gen_file_query
   gen_file_query_as_dict
   gen_search_query
   get_bad_files_verify_job
   get_checked_resources_verify_job
   get_checksum
   get_job_status
   get_metadata
   get_rcrs
   get_recall_job_id
   get_resource_id
   get_resource_path
   get_resource_permissions
   get_resource_size
   get_resource_tape
   get_resource_type
   get_result_verify_job
   get_search_status
   get_storage_information
   get_tag
   get_tape_barcode
   get_tape_id
   group_files_by_tape
   has_no_flag_partial
   has_no_flag_partial_details
   hsm2json
   hsm2json_dict
   hsm2json_file
   is_cached
   is_cached_details
   is_file
   is_job_finished
   is_job_processing
   is_job_queued
   is_job_successful
   is_namespace
   is_on_tape
   is_on_tape_details
   is_search_id
   is_search_incomplete
   is_search_successful
   is_tape_available
   job_exists
   job_queue
   json2hsm
   json_dict2hsm
   json_file2hsm
   json_str2hsm
   list
   list_clone_file
   list_clone_search
   login_valid
   ls
   makedirs
   mkdir
   move
   recall_dev
   recall_single
   rename
   resource_exists
   retrieve
   retrieve_improved
   run_slk
   search
   search_immediately
   searchid_exists
   session
   set_tag
   slk_helpers_version
   slk_version
   submit_verify_job
   tape_exists
   tape_status
   total_number_search_results
   try_archive
   valid_session
   valid_token
   version_slk
   version_slk_helpers


Classes provided by pyslk
============================================

Classes related to wrappers of ``slk`` and ``slk_helpers`` commands.

.. autosummary::
   :toctree: generated/

   FakeProc
   FileGroup
   GroupCollection
   StatusJob
   SubmittedJobs


Constants provided by pyslk
============================================

Costants.

.. autosummary::
   :toctree: generated/

   MAX_RETRIES_SUBMIT_JOBS_BASIC
   MAX_RETRIES_SUBMIT_JOBS_SAVE_MODE
   MIN_VERSION_SLK
   MIN_VERSION_SLK_HELPERS
   PYSLK_DEFAULT_LIST_CLONE_COLUMNS
   PYSLK_DEFAULT_LIST_COLUMNS
   PYSLK_DEFAULT_LIST_COLUMNS_3_3_21
   PYSLK_DEFAULT_LIST_COLUMN_TYPES
   PYSLK_DEFAULT_NUMBER_RETRIES_NONMOD_CMDS
   PYSLK_FILE_SIZE_UNITS
   PYSLK_LOGGER
   PYSLK_SEARCH_DELAY
   PYSLK_WILDCARDS
   SEARCH_QUERY_OPERATORS
   SLK
   SLK_HELPERS
   SLK_SYSTEM_CONFIG
   SLK_USER_CONFIG
   SLK_USER_LOG


Errors and warnings provided by pyslk
============================================

Errors and warnings.

.. autosummary::
   :toctree: generated/

   ArchiveError
   HostNotReachableError
   PySlkBadFileError
   PySlkBadProcessError
   PySlkEmptyInputError
   PySlkException
   PySlkNoValidLoginTokenError
   SizeMismatchError
   SlkIOError
   SlkWarning


Config
======

Adjust and query slk config.

.. autosummary::
   :toctree: generated/

   config.get
   config.set


Basic wrappers without proper parsing of the output
=========================================================

Basic wrappers for ``slk`` and ``slk_helpers`` commands.

.. autosummary::
   :toctree: generated/

   archive_raw
   checksum_raw
   chmod_raw
   delete_raw
   exists_raw
   gen_file_query_raw
   gen_search_query_raw
   group_files_by_tape_raw
   group_raw
   has_no_flag_partial_raw
   hostname_raw
   hsm2json_raw
   is_on_tape_raw
   iscached_raw
   job_exists_raw
   job_queue_raw
   job_report_raw
   job_status_raw
   json2hsm_raw
   list_clone_file_raw
   list_clone_search_raw
   list_raw
   metadata_raw
   mkdir_raw
   move_raw
   owner_raw
   print_rcrs_raw
   recall_raw
   recall2_raw
   rename_raw
   resource_path_raw
   resource_permissions_raw
   resource_tape_raw
   resource_type_raw
   result_verify_job_raw
   retrieve_raw
   retrieve2_raw
   search_immediately_raw
   search_incomplete_raw
   search_raw
   search_status_raw
   search_successful_raw
   searchid_exists_raw
   session_raw
   size_raw
   submit_verify_job_raw
   tag_raw
   tape_barcode_raw
   tape_exists_raw
   tape_id_raw
   tape_library_raw
   tape_status_raw
   tnsr_raw
   total_number_search_results_raw
   version_slk_helpers_raw
   version_slk_raw


Other functions provided by pyslk
============================================

Other functions.

.. autosummary::
   :toctree: generated/

   access_local
   check_for_errors
   construct_dst_from_src
   convert_expiration_date
   get_slk_pid
   hostname
   log_attempt_number
   module_load
   which

