Development
=============

Anyone is welcome to contribute to the code. To do so you must clone the
content of this repository and install the library using the following command:

.. code-block:: bash

    pip install -e .[tests]


This will install all requirements that are necessary for local development.
It is best practice to create a new branch before adding any changes.

.. code-block:: bash

    git checkout -b my-meaninful-branchname


After all changes have been committed and pushed back to the repository you can
submit the merge request that will be reviewed by the slk team.

Code conventions
-----------------

The gitlab test pipeline performs format checking. To check if the code is
still formatted correctly you can add a pre-commit hook that performs those
checks every time you are about to commit changes to the code. To add the
commit hook run the following command:

.. code-block:: bash

    pre-commit install

On ``git commit`` the ``pre-commit`` checks will be run. To run the checks
manually and print details on issues, please run this:

.. code-block:: bash

    pre-commit run --show-diff-on-failure


Prepare and create a release
---------------------------------

run tests:

.. code-block:: bash

    pytest -v tests

check codestyle:

.. code-block:: bash

    flake8 pyslk
    black --check -t py310 pyslk
    isort --check --profile black pyslk

make a release for PyPI (example for version 2.0.1):

.. code-block:: bash

    # ... do your changes ...
    # update changelog
    vim CHANGES.md         # update changes for the new version (doc/changelog.rst is generated later)
    # set tag and commit in git
    git tag 2.0.1        # tag version
    git commit -a        # commit changes
    git push --tags      # push changes including tag
    # build
    uv build .
    # have a look into subfolder 'dist'

make a conda package (example for version 2.0.1):

.. code-block:: bash

    # ... do your changes ...
    VERSION="2.0.1"
    # update changelog
    vim CHANGES.md       # update changes for the new version (doc/changelog.rst is generated later)
    vim doc/conf.py      # consider updating 'version_stable' and 'version_unstable' which refer to the versions installed on Levante
    # set tag and commit in git
    git tag ${VERSION}   # tag version
    git commit -a        # commit changes
    git push --tags      # push changes including tag
    # generate pypi package first
    uv build .
    # generate and update meta.yaml for conda build
    grayskull pypi dist/pyslk-${VERSION}.tar.gz --maintainers DanielHeydebreckDKRZ
    mv pyslk/meta.yaml .
    sed -i 's/PLEASE_ADD_LICENSE_FILE/LICENCE/g' meta.yaml
    sed -i -E 's/^(  license. ).*$/\1BSD-3-Clause/g' meta.yaml
    # build
    conda-build .          # build conda packages
    mkdir conda            # create folder to move conda packages to
    # move conda packages
    cp /opt/python/anaconda3/conda-bld/noarch/pyslk-${VERSION}-py_0.conda conda/

Generate documentation
-------------------------

.. code-block:: bash

    # install needed dependencies for doc generation
    pip3 install -U mock sphinx-book-theme numpydoc myst_parser
    # install this package without dependencies
    pip3 install --no-deps -e .
    # If dependencies are missing, please check whether all dependencies, which are listed in section
    # `project.optional-dependencies` under `doc`, are also installed here. I dependencies are missing here, please add
    # them.

    # Generate `pyslk.rst` and `modules.rst` files which contain only `:orphan:`. These files will be ignored by Sphinx
    # during the generation of the documentation. If they would not exist, sphinx-autodoc would generate them/
    if [ ! -e doc/pyslk.rst ]; then echo ":orphan:" > doc/pyslk.rst; fi
    if [ ! -e doc/modules.rst ]; then echo ":orphan:" > doc/modules.rst; fi
    cp CHANGES.md doc/
    # Prepare doc generation
    sphinx-apidoc -o doc pyslk
    # Generate documentation
    sphinx-build -b html doc build/html
    # you will find the documentation in `./build/html/index.txt`

.. note::

    Before generating the documentation the first time, the `doc/conf.py` was edited as follows:
    `sys.path.insert(0, os.path.abspath('..'))` was set and `'sphinx.ext.autodoc'` was inserted in the list
    `extensions`.
