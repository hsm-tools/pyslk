#!/bin/bash

black -t py310 pyslk
isort --profile black pyslk
flake8 pyslk
